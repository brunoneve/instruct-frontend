const endPoint = 'http://jsonplaceholder.typicode.com/users';
const colors = ["#1e73aa", "#2ab393", "#de6823", "#ccb726", "#7d0eab", "#de6823", "#443ea2"];

async function getUsers () {
    try {
        const response = await fetch(endPoint);
        const users = await response.json();

        return users;
    }
    catch(err) {
        console.error('Erro ao coletar dados da API!', err);
    }
}

async function domCards () {
    const users = await getUsers();
    const root = document.getElementById('list');

    users.map(user => {

        let initials = getInitials(user.name);

        const card = ` 
            <div class='column'>
                <div class='card'>
                    <header class="card-header">
                        <div class="avatar">${initials}</div>
                        <div class="user">
                            <h3 class="bn">${user.name}</h3>
                            <p>${user.username}</p>
                            <p class='email'>${user.email}</p>
                            <p class="site">www.${user.website}</p>
                        </div>
                    </header>

                    <footer class="more-details">
                        <p><strong>Telefone: </strong> ${user.phone}</p>
                        <p><strong>Endereço: </strong>${user.address.street}, ${user.address.suite}</p>
                        <p><strong>Cidade: </strong>${user.address.city} (${user.address.zipcode})</p>
                    <footer>
                </div>
            </div>
        `;
        root.innerHTML += card;
    });
    getColorAvatar();
}

/**
 * Recebe string com nome e retorna as duas primeiras letras
 * @param name 
 */
function getInitials (name) {
    let initials = name.match(/\b\w/g); // Separa string em arrays, pega somente primeira letra entre os espaços
    return ((initials.shift() || '') + (initials.pop() || '')).toUpperCase(); // pega Primeiro e ultimo valor do array
}

function getColorAvatar(){
    const avatars = document.querySelectorAll('.avatar');

    avatars.forEach(avatar => {
        const text = avatar.innerText;
        avatar.style.backgroundColor = colors[numberFromText(text) % colors.length];
    });
}

function numberFromText(text) {
    const charCodes = text.split('').map(char => char.charCodeAt(0)).join('');
    return parseInt(charCodes, 10);
};

function filterHost() {

    let input, filter, list, columns;
    input = document.getElementById('filter');
    filter = input.value.toUpperCase();
    list = document.getElementById("list");
    columns = list.getElementsByClassName('column');

    for (let i = 0; i < columns.length; i++) {
        let email = columns[i].getElementsByClassName('email')[0].textContent;
        let domain = email.substr(email.indexOf('@'));
        let extension = domain.substr(domain.indexOf('.'));

        columns[i].style.display = 'none';

        if (extension.toUpperCase().indexOf(filter) > -1) {
            columns[i].style.display = '';
        }
    }
}

domCards();